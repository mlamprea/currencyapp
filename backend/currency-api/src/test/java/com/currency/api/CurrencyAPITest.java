package com.currency.api;

import com.currency.business.PriceGenerator;
import com.currency.dao.CurrencyRepository;
import com.currency.entity.Currency;
import com.currency.model.CurrencyRates;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyAPITest {
    @Autowired
    private CurrencyRepository currencyRepository;

    @Test
    public void checkPricesGenerated() {
        PriceGenerator priceGenerator = new PriceGenerator();
        String rep = new PriceGenerator().generatePrice();
        assertTrue(!rep.equals(""));
    }

    @Test
    public void checkRatesGenerated() {

        List<Currency> currencies = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            currencies.add(new Currency(i + ""));
        }
        CurrencyRates currencyRates = new PriceGenerator().generateRates(currencies, "USD");
        assertTrue(currencies.size() > 0);
    }

    @Test
    public void checkPersistanceModule() {
        currencyRepository.save(new Currency("XYZ"));
        assertTrue(currencyRepository.findAll().stream().filter(currency -> currency.getSecurityCode().equals("XYZ")).count() > 0);
    }

}
