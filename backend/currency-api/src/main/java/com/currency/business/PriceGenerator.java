package com.currency.business;

import com.currency.entity.Currency;
import com.currency.model.CurrencyRates;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.*;

@Component
public class PriceGenerator {

    public static final int max = 5;
    public static final int min = 1;
    private Map<String, String> rates;


    public CurrencyRates generateRates(List<Currency> currencies, String id) {
        rates = new HashMap<>();
        long numHits = currencies.stream().filter(currency -> currency.getSecurityCode().equals(id.toUpperCase())).count();
        CurrencyRates currencyRates = new CurrencyRates();
        if (numHits == 0) {
            currencyRates.setBase(id);
            currencyRates.setDate(Calendar.getInstance().getTime().toString());
            currencyRates.setRates(rates);
            return currencyRates;
        }
        currencies.stream().filter(currency -> !currency.getSecurityCode().equals(id.toUpperCase())).forEach(currency -> rates.put(currency.getSecurityCode(), generatePrice()));

        currencyRates.setBase(id);
        currencyRates.setDate(Calendar.getInstance().getTime().toString());
        currencyRates.setRates(rates);
        return currencyRates;
    }

    public String generatePrice() {
        Random random = new Random();
        float price = random.nextInt((max - min) + 1) + min + random.nextFloat();
        return new DecimalFormat("##.0000").format(price);
    }


}
