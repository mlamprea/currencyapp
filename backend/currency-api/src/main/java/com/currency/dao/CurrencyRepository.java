package com.currency.dao;

import com.currency.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CurrencyRepository extends JpaRepository<Currency, String> {
   // List<Currency> findAllSimple();
}
