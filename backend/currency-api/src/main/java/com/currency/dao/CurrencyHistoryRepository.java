package com.currency.dao;

import com.currency.entity.CurrencyHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyHistoryRepository extends JpaRepository<CurrencyHistory, String> {
}
