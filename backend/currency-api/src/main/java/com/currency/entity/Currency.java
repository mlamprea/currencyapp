package com.currency.entity;


import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "currency")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Currency.findAll", query = "SELECT c FROM Currency c")
        , @NamedQuery(name = "Currency.findBySecurityCode", query = "SELECT c FROM Currency c WHERE c.securityCode = :securityCode")
        , @NamedQuery(name = "Currency.findByName", query = "SELECT c FROM Currency c WHERE c.name = :name")})
public class Currency implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "security_code")
    private String securityCode;
    @Column(name = "name")
    private String name;

    public Currency() {
    }

    public Currency(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (securityCode != null ? securityCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Currency)) {
            return false;
        }
        Currency other = (Currency) object;
        if ((this.securityCode == null && other.securityCode != null) || (this.securityCode != null && !this.securityCode.equals(other.securityCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Currency[ securityCode=" + securityCode + " ]";
    }

}
