package com.currency.entity;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Embeddable
public class CurrencyHistoryPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "base_source")
    private String baseSource;
    @Basic(optional = false)
    @Column(name = "base_target")
    private String baseTarget;
    @Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    public CurrencyHistoryPK() {
    }

    public CurrencyHistoryPK(String baseSource, String baseTarget, Date date) {
        this.baseSource = baseSource;
        this.baseTarget = baseTarget;
        this.date = date;
    }

    public String getBaseSource() {
        return baseSource;
    }

    public void setBaseSource(String baseSource) {
        this.baseSource = baseSource;
    }

    public String getBaseTarget() {
        return baseTarget;
    }

    public void setBaseTarget(String baseTarget) {
        this.baseTarget = baseTarget;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (baseSource != null ? baseSource.hashCode() : 0);
        hash += (baseTarget != null ? baseTarget.hashCode() : 0);
        hash += (date != null ? date.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CurrencyHistoryPK)) {
            return false;
        }
        CurrencyHistoryPK other = (CurrencyHistoryPK) object;
        if ((this.baseSource == null && other.baseSource != null) || (this.baseSource != null && !this.baseSource.equals(other.baseSource))) {
            return false;
        }
        if ((this.baseTarget == null && other.baseTarget != null) || (this.baseTarget != null && !this.baseTarget.equals(other.baseTarget))) {
            return false;
        }
        if ((this.date == null && other.date != null) || (this.date != null && !this.date.equals(other.date))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CurrencyHistoryPK[ baseSource=" + baseSource + ", baseTarget=" + baseTarget + ", date=" + date + " ]";
    }

}
