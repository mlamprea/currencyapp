package com.currency.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "currency_history")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "CurrencyHistory.findAll", query = "SELECT c FROM CurrencyHistory c")
        , @NamedQuery(name = "CurrencyHistory.findByBaseSource", query = "SELECT c FROM CurrencyHistory c WHERE c.currencyHistoryPK.baseSource = :baseSource")
        , @NamedQuery(name = "CurrencyHistory.findByBaseTarget", query = "SELECT c FROM CurrencyHistory c WHERE c.currencyHistoryPK.baseTarget = :baseTarget")
        , @NamedQuery(name = "CurrencyHistory.findByDate", query = "SELECT c FROM CurrencyHistory c WHERE c.currencyHistoryPK.date = :date")
        , @NamedQuery(name = "CurrencyHistory.findByPrice", query = "SELECT c FROM CurrencyHistory c WHERE c.price = :price")})
public class CurrencyHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CurrencyHistoryPK currencyHistoryPK;
    @Column(name = "price")
    private String price;

    public CurrencyHistory() {
    }

    public CurrencyHistory(CurrencyHistoryPK currencyHistoryPK) {
        this.currencyHistoryPK = currencyHistoryPK;
    }

    public CurrencyHistory(String baseSource, String baseTarget, Date date) {
        this.currencyHistoryPK = new CurrencyHistoryPK(baseSource, baseTarget, date);
    }

    public CurrencyHistoryPK getCurrencyHistoryPK() {
        return currencyHistoryPK;
    }

    public void setCurrencyHistoryPK(CurrencyHistoryPK currencyHistoryPK) {
        this.currencyHistoryPK = currencyHistoryPK;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (currencyHistoryPK != null ? currencyHistoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CurrencyHistory)) {
            return false;
        }
        CurrencyHistory other = (CurrencyHistory) object;
        if ((this.currencyHistoryPK == null && other.currencyHistoryPK != null) || (this.currencyHistoryPK != null && !this.currencyHistoryPK.equals(other.currencyHistoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CurrencyHistory[ currencyHistoryPK=" + currencyHistoryPK + " ]";
    }

}
