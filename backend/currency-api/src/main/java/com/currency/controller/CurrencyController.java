package com.currency.controller;

import com.currency.business.PriceGenerator;
import com.currency.dao.CurrencyHistoryRepository;
import com.currency.dao.CurrencyRepository;
import com.currency.entity.Currency;
import com.currency.model.CurrencyRates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


@RestController
class CurrencyController implements Serializable {

    @CrossOrigin
    @RequestMapping("/currency")
    Collection<Currency> getCurrency() {
        return currencyRepository.findAll();
    }


    @CrossOrigin
    @RequestMapping(value = "/currency/{id}", method = GET)
    @ResponseBody
    public CurrencyRates getCurrencyByID(@PathVariable("id") String id) {
        return priceGenerator.generateRates(currencyRepository.findAll(), id);
    }

    @Autowired
    private CurrencyHistoryRepository currencyHistoryRepository;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private PriceGenerator priceGenerator;

}
