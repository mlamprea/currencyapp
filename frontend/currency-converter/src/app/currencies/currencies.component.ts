import { Component, OnInit } from '@angular/core';
import { Currency } from '../model/currency'
import { CurrencyService } from '../service/currency.service';
import { MessageService } from '../service/message.service';

import { DOLLAR } from '../model/mockCurrencies';

@Component({
  selector: 'app-currencies',
  templateUrl: './currencies.component.html',
  styleUrls: ['./currencies.component.css']
})

export class CurrenciesComponent implements OnInit {

  priceSource: string;
  priceTarget: string;

  baseSource: string = "USD";
  baseTarget: string = "EUR";

  constructor(private currencyService: CurrencyService, private messageService: MessageService) { }

  ngOnInit() {
    //this.getInitialValueCurrencies();
    this.currencyService.getUnitCurrency(this.baseSource, this.baseTarget);
    this.currencyService.getUnitCurrencyBySubscription(this.baseSource, this.baseTarget);
  }

  convert(): void {
    try {
      if (typeof (this.priceSource) === 'undefined' || (typeof (this.priceSource === 'string') && (this.priceSource.length === 0))) {
        throw new Error('');
      }
      let result = this.currencyService.convert(Number(this.priceSource), this.baseSource, this.baseTarget);
      this.priceTarget = result.toString();
    } catch (error) {
      this.priceTarget = null;
      this.messageService.sendMessage(`Better check the value: ${error.message}`);
    }
  }

  getInitialValueCurrencies() {
    this.priceSource = '1';
    this.convert();
  }

}
