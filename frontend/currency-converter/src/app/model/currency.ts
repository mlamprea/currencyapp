export class Currency {
    constructor(public base: string, public rates: Map<string, number>) { }
}