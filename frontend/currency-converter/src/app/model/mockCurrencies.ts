import { Currency } from "./currency";

export const DOLLAR: Currency = new Currency("USD", new Map().set('EUR', 0.8).set('ILS', 3.5168));

