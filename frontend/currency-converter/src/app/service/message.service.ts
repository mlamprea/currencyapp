import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MessageService {

  constructor() { }

  alert = new Subject<string>();

  public sendMessage(message: string) {
    this.alert.next(message);
  }

}
