import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { of } from 'rxjs/observable/of';
import { Currency } from '../model/currency'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TemplateParseResult, typeSourceSpan } from '@angular/compiler';
import { isType } from '@angular/core/src/type';
import { environment } from '../../environments/environment.prod';


@Injectable()
export class CurrencyService {

  constructor(private http: HttpClient) { }

  unitConversion;
  currencyURL = environment.urlBackend;
  delayInMinutes: number = 10;

  getUnitCurrencyBySubscription(baseSource, baseTarget): void {
    // Consume API REST each 10 minutes
    const url = `${this.currencyURL}/${baseSource}`;
    Observable
      .interval(this.delayInMinutes * 60 * 1000)
      .timeInterval()
      .flatMap(() => this.http.get(url))
      .subscribe(c => this.unitConversion = c['rates'][baseTarget]);
  }

  getUnitCurrency(baseSource, baseTarget): void {
    console.log("url backend -> " + this.currencyURL);
    const url = `${this.currencyURL}/${baseSource}`;
    this.http.get(url).subscribe(c => this.unitConversion = c['rates'][baseTarget]);
  }


  convert(price: number, baseSource: string, baseTarget: string): number {
    if (!this.unitConversion) this.getUnitCurrency(baseSource, baseTarget);
    let result: number;
    try {
      result = Number(this.unitConversion) * price;
      if (isNaN(result)) {
        throw new Error('NaN');
      }
    } catch (error) {
      throw error;
    }
    return result;
  }
}