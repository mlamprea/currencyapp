import { Component, OnInit } from '@angular/core';
import { MessageService } from '../service/message.service';
import { Subject } from 'rxjs/Subject';
import { debounceTime } from 'rxjs/operator/debounceTime';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})

export class MessageComponent implements OnInit {

  constructor(public messageService: MessageService) { }

  staticAlertClosed = false;
  successMessage: string;

  ngOnInit(): void {
    setTimeout(() => this.staticAlertClosed = true, 3000);

    this.messageService.alert.subscribe((message) => this.successMessage = message);
    debounceTime.call(this.messageService.alert, 2000).subscribe(() => this.successMessage = null);
  }

}
